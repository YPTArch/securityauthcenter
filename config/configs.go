package config

import (
	"os"
	"fmt"
	"git.oschina.net/YPTArch/basic/sys/logs"
)
var (
	//数据库连接字符串
	DataBaseConnectionString string="root:abcd1234@tcp(192.168.1.235:3306)/SecurityAuthCenter?charset=utf8"
	//rpc 端口
	RpcPort string="11010"
	//http 端口
	HttpPort string="11011"
	//0 全部 1 rpc 2 http
	RunMode string="0"
)
func init() {
	logs.Logger.Info("----------加载系统环境变量----------")
	err:= LoadSysConfig()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(2)
	}
	logs.Logger.Info("----------系统环境变量----------")
	logs.Logger.Info("DataBaseConnectionString=>"+GetDataBaseConnectionString())
	logs.Logger.Info("HttpPort=>"+GetHttpPort())
	logs.Logger.Info("RpcPort=>"+GetRpcPort())
	logs.Logger.Info("RunMode=>"+GetRunMode())
	logs.Logger.Info("----------系统环境变量----------")
}
func LoadSysConfig() (error) {
	if os.Getenv("DataBaseConnectionString")!=""{
		DataBaseConnectionString=os.Getenv("DataBaseConnectionString")
	}
	if os.Getenv("HttpPort")!=""{
		HttpPort=os.Getenv("HttpPort")
	}
	if os.Getenv("RpcPort")!=""{
		RpcPort=os.Getenv("RpcPort")
	}
	if os.Getenv("RunMode")!=""{
		RunMode=os.Getenv("RunMode")
	}
	return nil
}
func GetDataBaseConnectionString() string {
	return DataBaseConnectionString
}
func GetHttpPort() string {
	return HttpPort
}

func GetRpcPort() string {
	return RpcPort
}

func GetRunMode() string {
	return RunMode
}