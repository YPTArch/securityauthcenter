package rpc

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"git.oschina.net/YPTArch/securityauthcenter/rpc/pd"
	"fmt"
	"io"
)

const (
	address     = "192.168.1.100:10010"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		fmt.Printf("did not connect: %v", err)
		return
	}
	defer conn.Close()
	c := pb.NewUserAuthAppClient(conn)
	// Contact the server and print out its response.
	_, err = c.Create(context.Background(), &pb.UserAuthAppRequest{UserId:"1",AppId:"7"})
	if err != nil {
		fmt.Printf("could not greet: %v", err)
	}
	stream,err:=c.GetUserAuthApp(context.Background(),&pb.GetUserAuthAppRequest{UserId:"1"})
	if err!=nil{
		fmt.Printf("could not greet: %v", err)
	}
	for{
		userAuthApp,err:=stream.Recv()
		if err==io.EOF{
			break
		}
		if err!=nil{
			fmt.Println("GetUserAuthApp()  "+ err.Error())
		}
		fmt.Println(userAuthApp)
	}

}
