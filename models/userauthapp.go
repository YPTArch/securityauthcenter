package models

import (
	"git.oschina.net/YPTArch/securityauthcenter/sys/db"
	"database/sql"
)
var UserAuthAppService *UserAuthApp = new(UserAuthApp)

type UserAuthApp struct {
	UserId   string    //用户id
	AppId    string    //应用id
}

func (*UserAuthApp) Create(userId,appId string)(error)  {
	sql:=`
			INSERT INTO UserAuthApp
			(UserId,AppId)
			VALUES
			(?,?)`
	err := db.MySqlDB.Exec(sql, userId,appId)
	if err != nil {
		return err
	}
	return nil
}

func (*UserAuthApp) GetUserAuthApp (userId string)([]UserAuthApp,error)  {
	sqlStr:=`	select UserId,AppId from UserAuthApp where  UserId=?`
	userAuthAppArr :=[]UserAuthApp{}

	rowHaandle := func(row *sql.Rows) error {
		userAuthApp:= UserAuthApp{}
		err := row.Scan(&userAuthApp.UserId, &userAuthApp.AppId)
		if err!=nil{
			return err
		}
		userAuthAppArr=append(userAuthAppArr,userAuthApp)
		return nil
	}
	err := db.MySqlDB.QueryRows(sqlStr,rowHaandle,userId)
	if err != nil {
		return nil,err
	}
	return userAuthAppArr,nil
}

